package main

import (
	"flag"
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"image/png"
	"log"
	"os"
	"sync"
)

//note What can be improved?
// Better filling dominating color
// Speedup background(green) filling(with waitgroups for example) or
// creating two slices - one with green coords, second with red
// than switching them depending on which of them are larger (one be bg, second mask or vice versa). Also with wg.
// Filling opened image, without creating additional images

type (
	coordinates struct {
		x, y int
	}

	coordsRange struct {
		xStart, xEnd int
		y            int
	}

	colors struct {
		red   color.RGBA
		green color.RGBA
	}
)

func main() {

	var output string

	flag.StringVar(&output, "output", "diff.png", "output filename")
	flag.StringVar(&output, "o", "diff.png", "output filename")
	flag.Parse()
	args := flag.Args()
	if len(args) != 2 {
		fmt.Println("usage: diff [<option>...] <image1> <image2>")
		os.Exit(1)
	}

	img1 := mustLoadImage(args[0])
	img2 := mustLoadImage(args[1])

	diff := createDiff(img1, img2)

	mustSaveImage(diff, output)
}

func createDiff(im1, im2 image.Image) image.Image {
	bounds := im1.Bounds()
	var rCoords []coordinates
	var gCount int
	for i := 0; i < bounds.Dx(); i++ {
		for j := 0; j < bounds.Dy(); j++ {
			if im1.At(i, j) != im2.At(i, j) {
				rCoords = append(rCoords, coordinates{i, j})
			} else {
				gCount += 1
			}
		}
	}
	return colorize(gCount, rCoords, bounds)
}

func colorize(gCount int, coords []coordinates, bounds image.Rectangle) image.Image {
	//drawing background
	colors := selectColor(gCount, len(coords))
	bg := image.NewRGBA(image.Rect(0, 0, bounds.Dx(), bounds.Dy()))
	draw.Draw(bg, bg.Bounds(), &image.Uniform{colors.green}, image.ZP, draw.Src)

	//dividing coordinates of differences into ranges and filling every range in its own goroutine
	mask := image.NewRGBA(image.Rect(0, 0, bounds.Dx(), bounds.Dy()))
	var wg sync.WaitGroup
	wg.Add(10)
	start, end := 0, 0
	coordsRangeLength := len(coords) / 10
	for i := 0; i < 10; i++ {
		//creating ranges
		end = start + coordsRangeLength
		if end > len(coords) {
			end = len(coords)
		}
		go fillPixels(coordsRange{start, end, bounds.Dy()}, coords, colors.red, mask, &wg)
		start = end
	}

	wg.Wait()
	//drawing mask
	draw.DrawMask(bg, bg.Bounds(), mask, image.ZP, mask, image.ZP, draw.Over)
	return bg
}

func fillPixels(cRange coordsRange, coords []coordinates, color color.RGBA, img *image.RGBA, wg *sync.WaitGroup) {
	for x := cRange.xStart; x < cRange.xEnd; x++ {
		img.Set(coords[x].x, coords[x].y, color) //red
	}
	wg.Done()
}

//selecting suitable transparency for mask
func selectColor(gCount, rCount int) colors {
	//red colors with different alphas
	colorsMap := []color.RGBA{
		{242, 38, 19, 255},
		{242, 38, 19, 251},
		{242, 38, 19, 249},
		{242, 38, 19, 247},
		{242, 38, 19, 245},
		{242, 38, 19, 243},
		{242, 38, 19, 241},
		{242, 38, 19, 239},
		{242, 38, 19, 237},
		{242, 38, 19, 235},
	}
	//computing index in slice of colors depending on subtraction result
	calcColor := func(max, min int) color.RGBA {
		ind := int(max / min)
		if ind > 10 {
			ind = 10
		}
		if min > max {
			return colorsMap[9-ind]
		}
		return colorsMap[ind]
	}
	return colors{red: calcColor(gCount, rCount),
		green: color.RGBA{38, 166, 91, 245}}
}

func mustLoadImage(filename string) image.Image {
	f := mustOpen(filename)
	defer f.Close()

	img, _, err := image.Decode(f)
	if err != nil {
		log.Fatal(err)
	}

	return img
}

func mustOpen(filename string) *os.File {
	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}

	return f
}

func mustSaveImage(img image.Image, output string) {
	f, err := os.OpenFile(output, os.O_WRONLY|os.O_CREATE, 0644)
	defer f.Close()
	if err != nil {
		log.Fatal(err)
	}

	png.Encode(f, img)
}

